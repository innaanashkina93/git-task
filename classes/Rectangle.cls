public class Rectangle {

    Integer a;
    Integer b;
    
    public void CreateRectangle(Integer a, Integer b)
    {
        this.a = a;
        this.b = b;
    }
    
    public string Square()
    {
         if(a != 0 && b != 0)
        {
            return String.valueOf(a*b);
        }
        else
        {
            return 'Стороны не заданы';
        }
        
    }
    
    public string Perimeter()
    {
        if(a != 0 && b != 0)
        {
            return String.valueOf(2*(a+b));
        }
        else
        {
            return 'Стороны не заданы';
        }
    }
    
}
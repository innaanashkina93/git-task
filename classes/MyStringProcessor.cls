public class MyStringProcessor implements StringProcessor{
    public String addPrefix (String str)
    {
        return 'pref_' + str;
    }
    
    public String addPostfix (String str){
        return 'postf_' + str;
    }
    
    public String removeWhitespaces (String str){
        return str.remove(' ');
    }
}